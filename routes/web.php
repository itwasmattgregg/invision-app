<?php

use App\Company;
use Illuminate\Support\Facades\Input;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// Auth::routes();
Route::get('/login', 'Auth\LoginController@showLoginForm' );
Route::post('/login', 'Auth\LoginController@login');
Route::post('/logout', 'Auth\LoginController@logout');


Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm');
Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail');

Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm');
Route::post('password/reset', 'Auth\ResetPasswordController@reset');

Route::get('/create-user', 'Auth\RegisterController@showRegistrationForm');
Route::post('/create-user', 'Auth\RegisterController@register');

Route::get('/home', 'HomeController@index');
Route::post('/home',
	['as' => 'upload_site', 'uses' => 'HomeController@uploadSite']);

Route::get('/company/{id}', 'CompanyController@show')->name('company');

Route::get('/sites', 'SitesController@index');

Route::delete('site/{id}', 'SitesController@destroy')->name('site.destroy');


Route::get('/{hash}', 'ClientController@landing');

Route::get('/{hash}/{any?}', function($hash = null, $any = null){
    $url = App\Site::where('hash', $hash)->first();
    if(empty($url)){
        abort(404);
    }
    $url = $url->url;
    if(empty($any)) {
        return redirect(sprintf('%s/index.html', $hash));
    }
    $fileToGrab = sprintf('%s/%s/%s', storage_path(), $url, $any);
    if(!file_exists($fileToGrab)) {
        abort(404);
    }
    return file_get_contents($fileToGrab);
})->where('any', '.*');
