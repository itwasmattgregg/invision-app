# Invision 
This is the beginnings of a project that could turn into a full client dashboard but for now it's purpose is simple: to allow the upload of invision prototypes for clients to view at a unique url.

## General Info
The app has a dashboard where you can create or update micro sites for an organization by uploading a zip file downloaded from invision. There is also a site list at `/sites` where all sites can be viewed with links.
## Getting Started
These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.
### Prerequisites
Composer:
https://getcomposer.org/download/

Yarn:
`npm install -g yarn`

Docker: 
Install the docker client on your computer
https://www.docker.com/products/overview

clone ST vagary repo:
`git@spydertrap.git.beanstalkapp.com:/spydertrap/vagary.git`
* then run `./start-base-services` from vagary root

Make sure you have,
```
127.0.0.1    postgres
127.0.0.1    adminer.dev
``` 
added to your localhosts file.
### Installing
In the root directory...

Run `composer install`

Run `yarn`

For the .env setup copy the .env.example file to .env and run `php artisan key:generate` to generate an app key for your .env file.

To configure the database go to http://adminer.dev, log into the mysql database, and create a database called invision. Then run `php artisan migrate --seed` in the project root.

Lastly add `127.0.0.1 invision.app` to your localhosts file and execute `docker-compose up -d` in the project root.

### Compiling assets and running FE dev server
To build your site run `yarn run watch` in the site root. This will start a basic web server wrapped around your invision.app localhost docker container. Meaning the frontend including browserSync will be running and updating via a webpack mix server thing and your backend will still be really coming from docker. It's a js server wrapped around docker.


## Deployment
### Staging
//add info here
### Production
## Built With / Dependencies
* Laravel
* Bootstrap
* SASS
* Bootstrap Select
* [Clipboard](https://clipboardjs.com/)
* [Zipper](https://github.com/Chumper/Zipper)



## Versioning
We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository].
## Authors & Contributors
* **Matt Gregg**
