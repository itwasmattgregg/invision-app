@extends('layouts.app')

@section('content')

@include('partials/delete-modal');

<div class="container">
    @if (session('status'))
        <div class="alert alert-success">
            {{ session('status') }}
        </div>
    @endif
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Sites (by company)</div>

                <div class="panel-body">

					@foreach ($companies->sortBy('name') as $company)
                        <h2>{{ $company->name }} <small> <a href="{{ route('company', ['id' => $company->id]) }}"> edit</a></small></h2>
                        @foreach ($company->sites as $site)
                            <div class="m-b-md">
                                <a href="/{{ $site->hash }}" class="site-url">{{ $site->name }}</a>
                                <span>
                                    {{ url('/') . '/' . $site->hash }}
                                </span>
                                <button class="copy-to-clip btn-link"><i class="glyphicon glyphicon-copy"></i></button>
                                {{ Form::open(array('route' => array('site.destroy', $site->id), 'method' => 'delete')) }}
                                    <button type="submit" class="btn btn-danger btn-xs">Delete</button>
                                {{ Form::close() }}
                            </div>
                        @endforeach
					@endforeach
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('appScripts')
    @parent
    <script src="https://cdnjs.cloudflare.com/ajax/libs/clipboard.js/1.5.16/clipboard.min.js"></script>
    <script>
    $(document).ready(function(){
        var clipboard = new Clipboard('.copy-to-clip', {
            target: function(trigger) {
                return trigger.previousElementSibling;
            }
        });

        clipboard.on('success', function(e) {
            $(e.trigger).tooltip({
                placement: 'bottom',
                title: 'Copied to Clipboard!',
                trigger: 'manual'
            })
            $(e.trigger).tooltip('show');
            setTimeout(function(){
                $(e.trigger).tooltip('hide');
            }, 1500);

            e.clearSelection();
        });
    });
    </script>

@endsection
