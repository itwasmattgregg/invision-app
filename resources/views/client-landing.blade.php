@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12 text-center">
            <h1>{{ $company->name }}</h1>
            <h4>{{ $site->name }}</h4>
            <p>
                Welcome to your individually prepared invision prototype. Click below to view.
            </p>
            <a href="{{ $site->hash }}/index.html" class="btn btn-primary">Enter</a>
        </div>
    </div>
</div>
@endsection

@section('appScripts')
    @parent


@endsection
