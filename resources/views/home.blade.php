@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    @if (count($errors) === 1)
                        {{ $errors->all()[0] }}
                    @else
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    @endif
                </div>
            @endif

            <div class="panel panel-default upload-site">
                <div class="panel-heading">Upload a new invision prototype</div>

                <div class="panel-body">
					{{ Form::open(array('route' => 'upload_site', 'class' => 'form', 'method'=> 'POST', 'files'=>true)) }}
						{{ csrf_field() }}

                        <div class="form-group">
                            <label>Select an existing company?</label><br>
                            <label>
                                {{ Form::radio('company-old', 'yes', true) }}
                                Yes
                            </label>
                            <label>
                                {{ Form::radio('company-old', 'no') }}
                                No
                            </label>
                        </div>

                        <div class="company-selector form-group">
    						{{ Form::label('company', 'Select Company') }}
    						{{ Form::select('company', $companies, null, ['class' => 'form-control selectpicker']) }}
                        </div>

                        <div class="company-text form-group display-none">
    						{{ Form::label('company-text', 'Name of Company') }}
    						{{ Form::text('company-text', $value = null, ['class' => 'form-control']) }}
                        </div>

                        <div class="form-group">
                            <label>Select an existing site?</label><br>
                            <label>
                                {{ Form::radio('site-old', 'yes') }}
                                Yes
                            </label>
                            <label>
                                {{ Form::radio('site-old', 'no', true) }}
                                No
                            </label>
                        </div>

                        <div class="site-selector form-group display-none">
                            {{ Form::label('site', 'Select Site to Replace') }}
                            {{ Form::select('site', [''=>''], null, ['class' => 'form-control selectpicker', 'disabled']) }}
                        </div>

                        <div class="site-text form-group">
    						{{ Form::label('site-text', 'Name of Site') }}
    						{{ Form::text('site-text', $value = null, ['class' => 'form-control']) }}
                        </div>

                        <div class="form-group file-area">
    						    {{ Form::file('zip_file', ['required' => 'required']) }}
                                <div class="file-dummy">
                                    <span class="success">Great! Your file is selected.</span>
                                    <span class="default">Drag the invision zip file here.</span>
                                </div>
    						<br />
                        </div>

                        <div class="submit-container">
                            <button class="btn btn-primary submit-js">Submit</button>
                            <button class="btn btn-default cancel-js">Cancel</button>
                            <input type="submit" name="submit" value="Confirm" class="btn btn-success confirm-js" />
                        </div>

					{{ Form::close() }}

                    @if (session('siteStatus'))
                        <div class="alert alert-success">
                            {{ session('siteStatus') }}
							<br>
							<a href="{{ url('/') }}/{{ session('link') }}/">{{ url('/') }}/{{ session('link') }}</a>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('appScripts')
    @parent

	<script>

	  $(document).ready(function(){

        //   Write a function to show a validation step before submit

            $('.selectpicker').selectpicker({
                liveSearch: true,
            });

            $('.submit-js').on('click', function(e){
                e.preventDefault();
                $('.submit-container').addClass('confirm');
            });

            $('.cancel-js').on('click', function(e){
                e.preventDefault();
                $('.submit-container').removeClass('confirm');
            });

            checkCompany();
            // checkSite();

            $('input[name="company-old"]').change(function(){
                checkCompany();
            });

            function checkCompany() {
                if($('input[name="company-old"]:checked').val() === 'yes') {
                    $('.company-selector').slideDown();
                    $('.company-text').slideUp();
                    $('input[name="site-old"][value="yes"]').prop('disabled', false);
                    // $('input[name="site-old"][value="yes"]').prop('checked', true);
                    // $('input[name="site-old"]').change();
                    $('.selectpicker').selectpicker('refresh');
                } else {
                    $('.company-selector').slideUp();
                    $('.company-text').slideDown();
                    $('input[name="site-old"][value="no"]').prop('checked', true);
                    $('input[name="site-old"]').change();
                    $('input[name="site-old"][value="yes"]').prop('disabled', true);
                }
                setTimeout(function(){
                    $('#company').change();
                }, 200);
            }

            $('input[name="site-old"]').change(function(){
                checkSite();
            });

            function checkSite() {
                if($('input[name="site-old"]:checked').val() === 'yes' && $('input[name="company-old"]:checked').val() === 'yes') {
                    $('.site-selector').slideDown();
                    $('.site-text').slideUp();
                } else {
                    $('.site-selector').slideUp();
                    $('.site-text').slideDown();
                }
            }

	        $('#company').change(function(){
                if($('input[name="company-old"]:checked').val() === 'yes') {
                    var company_id = parseInt($(this).val());
                    $.ajax({
                        type: "GET",
                        url: "/api/getsitesforcompany",
                        data: { "company_id": company_id },
                        cache: false,
                        success: function(data) {
                            var dropdown = $('#site');
                            dropdown.empty();
                            if(data.length > 0){
                                dropdown.prop('disabled', false);
                                $('input[name="site-old"][value="yes"]').prop('disabled', false);
                                $('input[name="site-old"][value="yes"]').prop('checked', true);
                                $('input[name="site-old"]').change();
                                $.each(data, function(index, element){
                                    dropdown.append("<option value=" + element.id + ">" + element.name + "</option>");
                                });
                                $('.selectpicker').selectpicker('refresh');
                            } else {
                                dropdown.prop('disabled', true);
                                $('input[name="site-old"][value="no"]').prop('checked', true);
                                $('input[name="site-old"]').change();
                                $('input[name="site-old"][value="yes"]').prop('disabled', true);
                                $('.selectpicker').selectpicker('refresh');
                            }

                        }
                    });
                }

	        });

	    });
	</script>

@endsection
