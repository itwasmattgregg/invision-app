import VueRouter    from 'vue-router'

//Define route components
// const Welcome = { template: '<div>welcome</div>' }
const Foo = { template: '<div>foo</div>' }
const Bar = { template: '<div>bar</div>' }

// lazy load components
const Welcome = (resolve) => require(['./components/App.vue'], resolve)

export default new VueRouter({
    mode: 'history',
    base: '/',
    routes: [
      { path: '/', component: Welcome },
      { path: '/foo', component: Foo },
      { path: '/bar', component: Bar }
    ]
});