<?php

namespace App\Http\Controllers;

use App\Company;
use App\Site;
use File;


class SitesController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
		$companies = Company::all();

        return view('sites', [
            'companies' => $companies
		]);
    }

    public function destroy($id)
    {
        $site = Site::find($id);
        $site->delete();

        $companySlug = str_slug($site->company->name, '-');
        $siteSlug = str_slug($site->name, '-');

        File::deleteDirectory(storage_path() . '/uploads/' . $companySlug . '/' . $siteSlug);

       return redirect('sites')->with(['status'=>'Site Deleted']);
    }
}
