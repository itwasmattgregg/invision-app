<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use File;
use App\Company;
use App\Site;
use Validator;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
		$companies = ['0'=>'Select a Company'] + Company::pluck('name', 'id')->all();

        // return this in order of recent used?
        return view('home', [
			'companies' => $companies
		]);
    }

	public function uploadSite(Request $request)
	{
        $this->validate($request, [
            'zip_file' => 'required',
            'company-text' => 'sometimes|unique:companies,name',
            'site-text' => 'sometimes|unique:sites,name',
        ]);

		$zipper = new \Chumper\Zipper\Zipper;
		$uploadPath = '/uploads/';

        if($request->input('company-old') == 'yes'){
            $companyId = $request->input('company');
            $companyName = Company::find($companyId)->name;
            $companySlug = str_slug($companyName, '-');
        } else {
            $companyName = $request->input('company-text');
            $companySlug = str_slug($companyName, '-');

            $company = new Company;
            $company->name = $companyName;
            $company->save();
            $companyId = $company->id;
        }

        if($request->input('site-old') == 'yes'){
            $siteId = $request->input('site');
            $siteName = Site::find($siteId)->name;
            $siteSlug = str_slug($siteName, '-');
        } else {
            $siteName = $request->input('site-text');
            $siteSlug = str_slug($siteName, '-');

            $site = new Site;
            $site->name = $siteName;
            $site->company_id = $companyId;
            $site->updated_by = Auth::user()->id;
            $site->save();
            $siteId = $site->id;
        }

		$file = $request->file('zip_file');

		if(!$file->isValid()){
			abort(500);
		}

        $site = Site::find($siteId);
        if(empty($site->hash)) {
            function findHash($hash) {
                if(Site::where('hash', $hash)->exists()) {
                    return findHash(str_random(10));
                } else {
                    return $hash;
                }
            }
            $hash = findHash(str_random(10));
        } else {
            $hash = $site->hash;
        }

        $site->hash = $hash;
        $site->url = $uploadPath . $companySlug . '/' . $siteSlug;
        $site->updated_by = Auth::user()->id;
        $site->save();


		if(!File::exists(storage_path() . $uploadPath . $companySlug . '/' . $siteSlug)) {
		    File::makeDirectory(storage_path() . $uploadPath . $companySlug . '/' . $siteSlug, 0777, true, true);
		}

		$zipper->make($file)->extractTo(storage_path() . $uploadPath . $companySlug . '/' . $siteSlug);

		// create a company in the database (if id doesn't exist)
		// company has:
		// * Name
		// * Slug

	    return redirect('home')->with(['siteStatus'=>'Site Created', 'link' => $hash]);
	}

}
