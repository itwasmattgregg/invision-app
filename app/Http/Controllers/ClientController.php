<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Site;
use App\Company;

class ClientController extends Controller
{
    /**
     * Show the landing page for the client
     *
     * @param  int  $id
     * @return Response
     */
    public function landing($hash)
    {
        $site = Site::where('hash', $hash)->first();
        if(empty($site)){
            abort(404);
        }
        $company = Company::where('id', $site->company_id)->first();

        return view('client-landing', ['site' => $site, 'company' => $company]);
    }
}
