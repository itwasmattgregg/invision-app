<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    //
	public function sites()
    {
        return $this->hasMany(Site::class);
    }
}
